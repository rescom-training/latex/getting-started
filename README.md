# Getting Started with LaTeX

Determine whether LaTeX is for you! During this free workshop you will typeset an article in LaTeX, learn how to insert graphics and work as a team to compete in a challenge.

LaTeX is a mark-up language focused on beautiful typesetting, and handles large, complex documents with ease. It is a standard tool in scientific and technical research fields, and is well-suited to the needs of people in the humanities.

To participate in the workshop, you will need:
- a computer (or laptop) with internet access and a web browser;
- Zoom installed on your computer - you can download Zoom for free from https://zoom.us/download;
- an Overleaf account - you can create one for free here: http://www.overleaf.com.

Prior to the workshop, please read our [Training Welcome Page](https://gitlab.unimelb.edu.au/rescom-training/latex/welcome). This page contains a training overview, eligibility & requirements, expectations, support, training format and dates & times.


## Accessing LaTeX

LaTeX is free and is open source software. It is available on Linux (as TeXLive), Mac (MacTeX), Windows (MiKTeX), and Online. Working online can be quick and easy: without installing anything, create your LaTeX document in a web browser then download the final PDF when its ready. However some may prefer to install LaTeX software on their own desktop or laptop computer. The strengths and weaknesses of both methods are discussed here to help you decide what's best for you: http://resbaz.github.io/resbook-installation/latex.

### Online
LaTeX can be used entirely online, using Overleaf: http://www.overleaf.com. All our trainings are run using Overleaf, so you are expected to have an Overleaf account before attending the training. If you do not already have an account, you can create one for free. 

The University of Melbourne is providing free Overleaf Professional accounts for all students, faculty and staff who would like to use a collaborative, online LaTeX editor for their projects. Overleaf Professional accounts provide real-time track changes, unlimited collaborators, and full document history. Upgrade to a pro Overleaf account by using a unimelb email address to create your Overleaf account. For more information see: https://www.overleaf.com/edu/unimelb.

I recommend viewing this guide to familiarise yourself the layout of Overleaf: [Overleaf Layout Guide](assets/overleaf.pdf).

### Offline
If you are interested in downloading a local distribution of LaTeX \(working offline\), I recommend that you follow Liantze's instructions on [Getting LaTeX to Work](http://liantze.penguinattack.org/getlatex.html). Once you have downloaded a [TeX distribution](https://www.latex-project.org/get/#tex-distributions), I recommend you choose a [LaTeX editor](https://beebom.com/best-latex-editors) \(there are many options to choose from - I recommend [TeXStudio](http://www.texstudio.org) or [TeXmaker](http://www.xm1math.net/texmaker)\).


## Workshop content

The _Getting Started with LaTeX_ slides can be accessed [here](assets/slides.pdf) (you can also view the original LaTeX document [here](https://www.overleaf.com/read/gbwdjvqtgyxv)).

During the workshop, activities will be run using Breakout rooms in Zoom. These are referred to as challenges, and can be viewed [here](challenges.md).

For more resources (including cheat sheets, websites and videos), please view [the Resources project on GitLab](https://gitlab.unimelb.edu.au/rescom-training/latex/resources).
