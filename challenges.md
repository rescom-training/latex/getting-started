# Challenges

## Who are you?

* Introduce yourselves.
* Come up with a team name!

## Getting started

* Select one member of your team to create an Overleaf document and share this with the group by pasting the edit link in the chat.
* Set your team name as the `\author{ }`.
* Cats vs Dogs: Are you team cats or team dogs?
* Decide which side you're on and give your `article` an appropriate `\title{ }`.

## Figures

* Insert an image of your animal using the `figure` environment.
* Give your image a clever and creative caption!
* When you return, share a link to your Overleaf document in the group chat.

Hint: Don't forget to use the `graphicx` package.
